import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("tests")
export class Test {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;
}