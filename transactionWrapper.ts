import { EntityManager, getManager, Repository } from "typeorm";
import { createNamespace, getNamespace } from "cls-hooked";

const NAMESPACE_NAME = "NAMESPACE_NAME";
const TYPE_ORM_KEY_PREFIX = "TYPE_ORM_KEY_PREFIX";

export const transactionWrapper = (func: any) => {
	const originalMethod = func;
	return function (...args: any[]) {
		const context = getNamespace(NAMESPACE_NAME)!;
		const connectionName: string = "default";

		const runWithNewTransaction = async () => {
			getManager(connectionName).transaction(async (entityManager) => {
				context.set(
					`${TYPE_ORM_KEY_PREFIX}${connectionName}`,
					entityManager
				);
				return await originalMethod(...args);
			});
		};
		return context.runAndReturn(runWithNewTransaction);
	};
};

export const initTransactionWrapper = () => {
	getNamespace(NAMESPACE_NAME) || createNamespace(NAMESPACE_NAME);

	Object.defineProperty(Repository.prototype, "manager", {
		get() {
			const context = getNamespace(NAMESPACE_NAME);

			if (context && context.active) {
				return (
					context.get(
						`${TYPE_ORM_KEY_PREFIX}${this._connectionName}`
					) || this._manager
				);
			}
			return this._manager || getManager(this._connectionName);
		},
		set(manager: EntityManager | undefined) {
			this._manager = manager;
			this._connectionName = manager?.connection?.name;
		},
	});
};
