import "reflect-metadata";

import {
	initTransactionWrapper,
	transactionWrapper,
} from "./transactionWrapper";
import { createConnection, getRepository } from "typeorm";
import { Test } from "./entity";
import {
	Transactional,
	initializeTransactionalContext,
	patchTypeORMRepositoryWithBaseRepository,
} from "typeorm-transactional-cls-hooked";

const func = async (time: number) => {
	await getRepository(Test).insert({ name: "Rostik" });
	await getRepository(Test).insert({ name: "Rostik" });
	await new Promise((r) => {
		setTimeout(async () => {
			await getRepository(Test).insert({ name: "Rostik" });
			r(1);
		}, time);
	});
};

const wrapFunc = <TFunction extends Function>(func: TFunction) => {
	const d = { value: func };
	Transactional()({}, "", d);
	return d.value as TFunction;
};

const wrapFuncUsingClass = <TFunction extends Function>(func: TFunction) => {
	class Temp {
		@Transactional()
		async methodToCallFunc(...args: any) {
			await func(...args);
		}
	}

	const tempInstance = new Temp();

	return (tempInstance.methodToCallFunc as unknown) as TFunction;
};

const workWithLib = () => {
	initializeTransactionalContext();
	patchTypeORMRepositoryWithBaseRepository();

	// WORKS
	const wrappedFunction = wrapFunc(func);
	wrappedFunction(1000);

	// WORKS TO
	const wrapperFunctionWithClass = wrapFuncUsingClass(func);
	wrapperFunctionWithClass(3000);
};

const workCustom = () => {
	initTransactionWrapper();

	const wrappedFuncWithCustomTransactionWrapper = transactionWrapper(func);
	wrappedFuncWithCustomTransactionWrapper(5000);
};

createConnection().then(async () => {
	// ONLY ONE CAN WORK
	const tryCustom = true;

	if (tryCustom) workCustom();
	else workWithLib();
});
